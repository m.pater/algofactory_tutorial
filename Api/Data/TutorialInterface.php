<?php
/**
 * Copyright ©  All rights reserved.
 * See COPYING.txt for license details.
 */
declare(strict_types=1);

namespace AlgoFactory\Tutorial\Api\Data;

interface TutorialInterface extends \Magento\Framework\Api\ExtensibleDataInterface
{

    const NAME = 'name';
    const URL = 'url';
    const TUTORIAL_ID = 'tutorial_id';

    /**
     * Get tutorial_id
     * @return string|null
     */
    public function getTutorialId();

    /**
     * Set tutorial_id
     * @param string $tutorialId
     * @return \AlgoFactory\Tutorial\Api\Data\TutorialInterface
     */
    public function setTutorialId($tutorialId);

    /**
     * Get name
     * @return string|null
     */
    public function getName();

    /**
     * Set name
     * @param string $name
     * @return \AlgoFactory\Tutorial\Api\Data\TutorialInterface
     */
    public function setName($name);

    /**
     * Retrieve existing extension attributes object or create a new one.
     * @return \AlgoFactory\Tutorial\Api\Data\TutorialExtensionInterface|null
     */
    public function getExtensionAttributes();

    /**
     * Set an extension attributes object.
     * @param \AlgoFactory\Tutorial\Api\Data\TutorialExtensionInterface $extensionAttributes
     * @return $this
     */
    public function setExtensionAttributes(
        \AlgoFactory\Tutorial\Api\Data\TutorialExtensionInterface $extensionAttributes
    );
}

