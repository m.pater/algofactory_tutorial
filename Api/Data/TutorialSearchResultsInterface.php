<?php
/**
 * Copyright ©  All rights reserved.
 * See COPYING.txt for license details.
 */
declare(strict_types=1);

namespace AlgoFactory\Tutorial\Api\Data;

interface TutorialSearchResultsInterface extends \Magento\Framework\Api\SearchResultsInterface
{

    /**
     * Get Tutorial list.
     * @return \AlgoFactory\Tutorial\Api\Data\TutorialInterface[]
     */
    public function getItems();

    /**
     * Set name list.
     * @param \AlgoFactory\Tutorial\Api\Data\TutorialInterface[] $items
     * @return $this
     */
    public function setItems(array $items);
}

