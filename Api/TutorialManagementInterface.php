<?php
/**
 * Copyright ©  All rights reserved.
 * See COPYING.txt for license details.
 */
declare(strict_types=1);

namespace AlgoFactory\Tutorial\Api;

interface TutorialManagementInterface
{

    /**
     * GET for Tutorial api
     * @param string $param
     * @return string
     */
    public function getTutorial($param);

    /**
     * POST for Tutorial api
     * @param string $param
     * @return string
     */
    public function postTutorial($param);

    /**
     * DELETE for Tutorial api
     * @param string $param
     * @return string
     */
    public function deleteTutorial($param);
}

