<?php
/**
 * Copyright ©  All rights reserved.
 * See COPYING.txt for license details.
 */
declare(strict_types=1);

namespace AlgoFactory\Tutorial\Api;

use Magento\Framework\Api\SearchCriteriaInterface;

interface TutorialRepositoryInterface
{

    /**
     * Save Tutorial
     * @param \AlgoFactory\Tutorial\Api\Data\TutorialInterface $tutorial
     * @return \AlgoFactory\Tutorial\Api\Data\TutorialInterface
     * @throws \Magento\Framework\Exception\LocalizedException
     */
    public function save(
        \AlgoFactory\Tutorial\Api\Data\TutorialInterface $tutorial
    );

    /**
     * Retrieve Tutorial
     * @param string $tutorialId
     * @return \AlgoFactory\Tutorial\Api\Data\TutorialInterface
     * @throws \Magento\Framework\Exception\LocalizedException
     */
    public function get($tutorialId);

    /**
     * Retrieve Tutorial matching the specified criteria.
     * @param \Magento\Framework\Api\SearchCriteriaInterface $searchCriteria
     * @return \AlgoFactory\Tutorial\Api\Data\TutorialSearchResultsInterface
     * @throws \Magento\Framework\Exception\LocalizedException
     */
    public function getList(
        \Magento\Framework\Api\SearchCriteriaInterface $searchCriteria
    );

    /**
     * Delete Tutorial
     * @param \AlgoFactory\Tutorial\Api\Data\TutorialInterface $tutorial
     * @return bool true on success
     * @throws \Magento\Framework\Exception\LocalizedException
     */
    public function delete(
        \AlgoFactory\Tutorial\Api\Data\TutorialInterface $tutorial
    );

    /**
     * Delete Tutorial by ID
     * @param string $tutorialId
     * @return bool true on success
     * @throws \Magento\Framework\Exception\NoSuchEntityException
     * @throws \Magento\Framework\Exception\LocalizedException
     */
    public function deleteById($tutorialId);
}

