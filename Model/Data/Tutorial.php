<?php
/**
 * Copyright ©  All rights reserved.
 * See COPYING.txt for license details.
 */
declare(strict_types=1);

namespace AlgoFactory\Tutorial\Model\Data;

use AlgoFactory\Tutorial\Api\Data\TutorialInterface;

class Tutorial extends \Magento\Framework\Api\AbstractExtensibleObject implements TutorialInterface
{

    /**
     * Get tutorial_id
     * @return string|null
     */
    public function getTutorialId()
    {
        return $this->_get(self::TUTORIAL_ID);
    }

    /**
     * Set tutorial_id
     * @param string $tutorialId
     * @return \AlgoFactory\Tutorial\Api\Data\TutorialInterface
     */
    public function setTutorialId($tutorialId)
    {
        return $this->setData(self::TUTORIAL_ID, $tutorialId);
    }

    /**
     * Get name
     * @return string|null
     */
    public function getName()
    {
        return $this->_get(self::NAME);
    }

    /**
     * Set name
     * @param string $name
     * @return \AlgoFactory\Tutorial\Api\Data\TutorialInterface
     */
    public function setName($name)
    {
        return $this->setData(self::NAME, $name);
    }

    /**
     * Get url
     * @return string|null
     */
    public function getUrl()
    {
        return $this->_get(self::NAME);
    }

    /**
     * Set url
     * @param string $url
     * @return \AlgoFactory\Tutorial\Api\Data\TutorialInterface
     */
    public function setUrl($url)
    {
        return $this->setData(self::URL, $url);
    }

    /**
     * Retrieve existing extension attributes object or create a new one.
     * @return \AlgoFactory\Tutorial\Api\Data\TutorialExtensionInterface|null
     */
    public function getExtensionAttributes()
    {
        return $this->_getExtensionAttributes();
    }

    /**
     * Set an extension attributes object.
     * @param \AlgoFactory\Tutorial\Api\Data\TutorialExtensionInterface $extensionAttributes
     * @return $this
     */
    public function setExtensionAttributes(
        \AlgoFactory\Tutorial\Api\Data\TutorialExtensionInterface $extensionAttributes
    ) {
        return $this->_setExtensionAttributes($extensionAttributes);
    }
}

