<?php
/**
 * Copyright ©  All rights reserved.
 * See COPYING.txt for license details.
 */
declare(strict_types=1);

namespace AlgoFactory\Tutorial\Model\ResourceModel;

class Tutorial extends \Magento\Framework\Model\ResourceModel\Db\AbstractDb
{

    /**
     * Define resource model
     *
     * @return void
     */
    protected function _construct()
    {
        $this->_init('algofactory_tutorial_tutorial', 'tutorial_id');
    }
}

