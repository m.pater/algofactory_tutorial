<?php
/**
 * Copyright ©  All rights reserved.
 * See COPYING.txt for license details.
 */
declare(strict_types=1);

namespace AlgoFactory\Tutorial\Model;

use AlgoFactory\Tutorial\Api\Data\TutorialInterface;
use AlgoFactory\Tutorial\Api\Data\TutorialInterfaceFactory;
use Magento\Framework\Api\DataObjectHelper;

class Tutorial extends \Magento\Framework\Model\AbstractModel
{

    protected $tutorialDataFactory;

    protected $dataObjectHelper;

    protected $_eventPrefix = 'algofactory_tutorial_tutorial';

    /**
     * @param \Magento\Framework\Model\Context $context
     * @param \Magento\Framework\Registry $registry
     * @param TutorialInterfaceFactory $tutorialDataFactory
     * @param DataObjectHelper $dataObjectHelper
     * @param \AlgoFactory\Tutorial\Model\ResourceModel\Tutorial $resource
     * @param \AlgoFactory\Tutorial\Model\ResourceModel\Tutorial\Collection $resourceCollection
     * @param array $data
     */
    public function __construct(
        \Magento\Framework\Model\Context $context,
        \Magento\Framework\Registry $registry,
        TutorialInterfaceFactory $tutorialDataFactory,
        DataObjectHelper $dataObjectHelper,
        \AlgoFactory\Tutorial\Model\ResourceModel\Tutorial $resource,
        \AlgoFactory\Tutorial\Model\ResourceModel\Tutorial\Collection $resourceCollection,
        array $data = []
    ) {
        $this->tutorialDataFactory = $tutorialDataFactory;
        $this->dataObjectHelper = $dataObjectHelper;
        parent::__construct($context, $registry, $resource, $resourceCollection, $data);
    }

    /**
     * Retrieve tutorial model with tutorial data
     * @return TutorialInterface
     */
    public function getDataModel()
    {
        $tutorialData = $this->getData();
        
        $tutorialDataObject = $this->tutorialDataFactory->create();
        $this->dataObjectHelper->populateWithArray(
            $tutorialDataObject,
            $tutorialData,
            TutorialInterface::class
        );
        
        return $tutorialDataObject;
    }
}

