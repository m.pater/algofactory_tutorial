<?php
/**
 * Copyright ©  All rights reserved.
 * See COPYING.txt for license details.
 */
declare(strict_types=1);

namespace AlgoFactory\Tutorial\Model;

class TutorialManagement implements \AlgoFactory\Tutorial\Api\TutorialManagementInterface
{

    /**
     * {@inheritdoc}
     */
    public function getTutorial($param)
    {
        return 'hello api GET return the $param ' . $param;
    }

    /**
     * {@inheritdoc}
     */
    public function postTutorial($param)
    {
        return 'hello api POST return the $param ' . $param;
    }

    /**
     * {@inheritdoc}
     */
    public function deleteTutorial($param)
    {
        return 'hello api DELETE return the $param ' . $param;
    }
}

