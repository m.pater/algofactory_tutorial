<?php
/**
 * Copyright ©  All rights reserved.
 * See COPYING.txt for license details.
 */
declare(strict_types=1);

namespace AlgoFactory\Tutorial\Model;

use AlgoFactory\Tutorial\Api\Data\TutorialInterfaceFactory;
use AlgoFactory\Tutorial\Api\Data\TutorialSearchResultsInterfaceFactory;
use AlgoFactory\Tutorial\Api\TutorialRepositoryInterface;
use AlgoFactory\Tutorial\Model\ResourceModel\Tutorial as ResourceTutorial;
use AlgoFactory\Tutorial\Model\ResourceModel\Tutorial\CollectionFactory as TutorialCollectionFactory;
use Magento\Framework\Api\DataObjectHelper;
use Magento\Framework\Api\ExtensibleDataObjectConverter;
use Magento\Framework\Api\ExtensionAttribute\JoinProcessorInterface;
use Magento\Framework\Api\SearchCriteria\CollectionProcessorInterface;
use Magento\Framework\Exception\CouldNotDeleteException;
use Magento\Framework\Exception\CouldNotSaveException;
use Magento\Framework\Exception\NoSuchEntityException;
use Magento\Framework\Reflection\DataObjectProcessor;
use Magento\Store\Model\StoreManagerInterface;

class TutorialRepository implements TutorialRepositoryInterface
{

    protected $resource;

    protected $tutorialFactory;

    protected $tutorialCollectionFactory;

    protected $searchResultsFactory;

    protected $dataObjectHelper;

    protected $dataObjectProcessor;

    protected $dataTutorialFactory;

    protected $extensionAttributesJoinProcessor;

    private $storeManager;

    private $collectionProcessor;

    protected $extensibleDataObjectConverter;

    /**
     * @param ResourceTutorial $resource
     * @param TutorialFactory $tutorialFactory
     * @param TutorialInterfaceFactory $dataTutorialFactory
     * @param TutorialCollectionFactory $tutorialCollectionFactory
     * @param TutorialSearchResultsInterfaceFactory $searchResultsFactory
     * @param DataObjectHelper $dataObjectHelper
     * @param DataObjectProcessor $dataObjectProcessor
     * @param StoreManagerInterface $storeManager
     * @param CollectionProcessorInterface $collectionProcessor
     * @param JoinProcessorInterface $extensionAttributesJoinProcessor
     * @param ExtensibleDataObjectConverter $extensibleDataObjectConverter
     */
    public function __construct(
        ResourceTutorial $resource,
        TutorialFactory $tutorialFactory,
        TutorialInterfaceFactory $dataTutorialFactory,
        TutorialCollectionFactory $tutorialCollectionFactory,
        TutorialSearchResultsInterfaceFactory $searchResultsFactory,
        DataObjectHelper $dataObjectHelper,
        DataObjectProcessor $dataObjectProcessor,
        StoreManagerInterface $storeManager,
        CollectionProcessorInterface $collectionProcessor,
        JoinProcessorInterface $extensionAttributesJoinProcessor,
        ExtensibleDataObjectConverter $extensibleDataObjectConverter
    ) {
        $this->resource = $resource;
        $this->tutorialFactory = $tutorialFactory;
        $this->tutorialCollectionFactory = $tutorialCollectionFactory;
        $this->searchResultsFactory = $searchResultsFactory;
        $this->dataObjectHelper = $dataObjectHelper;
        $this->dataTutorialFactory = $dataTutorialFactory;
        $this->dataObjectProcessor = $dataObjectProcessor;
        $this->storeManager = $storeManager;
        $this->collectionProcessor = $collectionProcessor;
        $this->extensionAttributesJoinProcessor = $extensionAttributesJoinProcessor;
        $this->extensibleDataObjectConverter = $extensibleDataObjectConverter;
    }

    /**
     * {@inheritdoc}
     */
    public function save(
        \AlgoFactory\Tutorial\Api\Data\TutorialInterface $tutorial
    ) {
        /* if (empty($tutorial->getStoreId())) {
            $storeId = $this->storeManager->getStore()->getId();
            $tutorial->setStoreId($storeId);
        } */
        
        $tutorialData = $this->extensibleDataObjectConverter->toNestedArray(
            $tutorial,
            [],
            \AlgoFactory\Tutorial\Api\Data\TutorialInterface::class
        );
        
        $tutorialModel = $this->tutorialFactory->create()->setData($tutorialData);
        
        try {
            $this->resource->save($tutorialModel);
        } catch (\Exception $exception) {
            throw new CouldNotSaveException(__(
                'Could not save the tutorial: %1',
                $exception->getMessage()
            ));
        }
        return $tutorialModel->getDataModel();
    }

    /**
     * {@inheritdoc}
     */
    public function get($tutorialId)
    {
        $tutorial = $this->tutorialFactory->create();
        $this->resource->load($tutorial, $tutorialId);
        if (!$tutorial->getId()) {
            throw new NoSuchEntityException(__('Tutorial with id "%1" does not exist.', $tutorialId));
        }
        return $tutorial->getDataModel();
    }

    /**
     * {@inheritdoc}
     */
    public function getList(
        \Magento\Framework\Api\SearchCriteriaInterface $criteria
    ) {
        $collection = $this->tutorialCollectionFactory->create();
        
        $this->extensionAttributesJoinProcessor->process(
            $collection,
            \AlgoFactory\Tutorial\Api\Data\TutorialInterface::class
        );
        
        $this->collectionProcessor->process($criteria, $collection);
        
        $searchResults = $this->searchResultsFactory->create();
        $searchResults->setSearchCriteria($criteria);
        
        $items = [];
        foreach ($collection as $model) {
            $items[] = $model->getDataModel();
        }
        
        $searchResults->setItems($items);
        $searchResults->setTotalCount($collection->getSize());
        return $searchResults;
    }

    /**
     * {@inheritdoc}
     */
    public function delete(
        \AlgoFactory\Tutorial\Api\Data\TutorialInterface $tutorial
    ) {
        try {
            $tutorialModel = $this->tutorialFactory->create();
            $this->resource->load($tutorialModel, $tutorial->getTutorialId());
            $this->resource->delete($tutorialModel);
        } catch (\Exception $exception) {
            throw new CouldNotDeleteException(__(
                'Could not delete the Tutorial: %1',
                $exception->getMessage()
            ));
        }
        return true;
    }

    /**
     * {@inheritdoc}
     */
    public function deleteById($tutorialId)
    {
        return $this->delete($this->get($tutorialId));
    }
}

