# Mage2 Module AlgoFactory Tutorial

    ``algofactory/module-tutorial``

 - [Main Functionalities](#markdown-header-main-functionalities)
 - [Installation](#markdown-header-installation)
 - [Configuration](#markdown-header-configuration)
 - [Specifications](#markdown-header-specifications)
 - [Attributes](#markdown-header-attributes)


## Main Functionalities
Tutoriel Magento By Algo Factory

## Installation
\* = in production please use the `--keep-generated` option

### Type 1: Zip file

 - Unzip the zip file in `app/code/AlgoFactory`
 - Enable the module by running `php bin/magento module:enable AlgoFactory_Tutorial`
 - Apply database updates by running `php bin/magento setup:upgrade`\*
 - Flush the cache by running `php bin/magento cache:flush`

### Type 2: Composer

 - Make the module available in a composer repository for example:
    - private repository `repo.magento.com`
    - public repository `packagist.org`
    - public github repository as vcs
 - Add the composer repository to the configuration by running `composer config repositories.repo.magento.com composer https://repo.magento.com/`
 - Install the module composer by running `composer require algofactory/module-tutorial`
 - enable the module by running `php bin/magento module:enable AlgoFactory_Tutorial`
 - apply database updates by running `php bin/magento setup:upgrade`\*
 - Flush the cache by running `php bin/magento cache:flush`


## Configuration




## Specifications

 - API Endpoint
	- GET - AlgoFactory\Tutorial\Api\TutorialManagementInterface > AlgoFactory\Tutorial\Model\TutorialManagement

 - API Endpoint
	- POST - AlgoFactory\Tutorial\Api\TutorialManagementInterface > AlgoFactory\Tutorial\Model\TutorialManagement

 - API Endpoint
	- DELETE - AlgoFactory\Tutorial\Api\TutorialManagementInterface > AlgoFactory\Tutorial\Model\TutorialManagement

 - Controller
	- adminhtml > algofactory_tutorial/index/index

 - Model
	- Tutorial


## Attributes



